package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bbs.beans.Message;
import bbs.beans.UserMessage;
import bbs.dao.MessageDao;
import bbs.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String strStart, String strEnd, String keywordCategory) {
    	final int LIMIT_NUM = 1000;
		Connection connection = null;

		String startDate ="";
		String endDate = "";

        if(strStart == null || strStart.isEmpty()) {
        	startDate = "2020-01-01 00:00:00";
        }else {
        	startDate = strStart  + " 00:00:00";
        }

        if(strEnd == null || strEnd.isEmpty()) {
        	//時間を取得（フォーマット要修正
        	Date rowDate =new Date();
        	SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	endDate = sdf.format(rowDate);
        }else {
        	endDate = strEnd  + " 23:59:59";
        }

        //startDate/endDateをdaoに渡す

        if (keywordCategory == null) {
        	keywordCategory = "";
		}

        try {
            connection = getConnection();
            List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, keywordCategory, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(int id) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}