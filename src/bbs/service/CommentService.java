package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.Comment;
import bbs.beans.UserComment;
import bbs.dao.CommentDao;
import bbs.dao.UserCommentDao;

public class CommentService {

    public void insert(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().insert(connection, comment);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserComment> select() {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            List<UserComment> comments = new UserCommentDao().select(connection, LIMIT_NUM);
            commit(connection);

            return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void commentDelete(int commentId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().commentDelete(connection, commentId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}