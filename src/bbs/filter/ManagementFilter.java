package bbs.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;

@WebFilter(urlPatterns={"/management", "/setting", "/signup"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

	      HttpSession session = ((HttpServletRequest)request).getSession();
	      User user = (User) session.getAttribute("loginUser");

		List<String> errorMessages = new ArrayList<String>();
	      int branchId = user.getBranchId();
	      int departmentId = user.getDepartmentId();

	      if ( branchId != 1 && departmentId != 1) {

	    	  	errorMessages.add("<WARNING> 権限がありません：管理者のみが閲覧できるページです");
	        	 session.setAttribute("errorMessages", errorMessages);
		    	  	((HttpServletResponse)response).sendRedirect("./");
	      }else {
	    	  chain.doFilter(request, response);
	      }
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}