package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserBranchDepartment;
import bbs.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

    public List<UserBranchDepartment> select(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append(" users.id AS id, ");
            sql.append(" users.account AS account, ");
            sql.append(" users.name AS name, ");
            sql.append(" users.branch_id AS branch_id, ");
            sql.append(" branches.name AS branch_name, ");
            sql.append(" users.department_id AS department_id, ");
            sql.append(" departments.name AS department_name, ");
            sql.append(" users.is_stopped AS is_stopped, ");
            sql.append(" users.created_date AS created_date, ");
            sql.append(" users.updated_date AS updated_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserBranchDepartment> users = toUserBranchDepartment(rs);
            return users;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

        List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
        try {
            while (rs.next()) {
                UserBranchDepartment user = new UserBranchDepartment();
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setName(rs.getString("name"));
                user.setBranchId(rs.getInt("branch_id"));
                user.setDepartmentId(rs.getInt("department_id"));
                user.setBranchName(rs.getString("branch_name"));
                user.setDepartmentName(rs.getString("department_name"));
                user.setIsStopped(rs.getInt("is_stopped"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

               users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }
}

