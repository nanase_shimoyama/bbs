package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserMessage;
import bbs.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, String startDate, String endDate, String keywordCategory, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    messages.id as id, ");
            sql.append("    users.name as name, ");
            sql.append("    messages.user_id as user_id, ");
            sql.append(" users.account AS account,");
            sql.append("    messages.title as title, ");
            sql.append("    messages.category as category, ");
            sql.append("    messages.text as text, ");
            sql.append("    messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date between ? and ? ");
			sql.append("AND category LIKE ?");
            sql.append("ORDER BY created_date DESC limit " + num);
            ps = connection.prepareStatement(sql.toString());

			ps.setString(1,startDate);
			ps.setString(2,endDate);
			ps.setString(3, "%" + keywordCategory + "%");

            ResultSet rs = ps.executeQuery();
            List<UserMessage> messages = toUserMessage(rs);
            return messages;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessage(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setName(rs.getString("name"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setTitle(rs.getString("title"));
                message.setCategory(rs.getString("category"));
                message.setText(rs.getString("text"));
                message.setCreatedDate(rs.getTimestamp("created_date"));
                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}