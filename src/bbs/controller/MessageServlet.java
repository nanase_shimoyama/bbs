package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.User;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws IOException, ServletException {

	        request.getRequestDispatcher("message.jsp").forward(request, response);
	    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {


		HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		Message message = new Message();
		message.setTitle(title);
		message.setCategory(category);
		message.setText(text);

		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());

		if (!isValid(title,category,text,errorMessages)) {
	            session.setAttribute("errorMessages", errorMessages);
	            request.getRequestDispatcher("message.jsp").forward(request, response);
	            return;
	        }

        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    private boolean isValid(String title, String category, String text, List<String> errorMessages) {

        if (StringUtils.isEmpty(title) == true) {
        	errorMessages.add("タイトルを入力してください (30文字以下)");
        } else if (title.matches("^[  |　]+$")) {
        	errorMessages.add("件名を入力してください");
        } else if (30 < title.length()) {
        	errorMessages.add("入力値を超えています (30文字以下で入力してください)");
        }

        if (StringUtils.isEmpty(category) == true) {
        	errorMessages.add("カテゴリを入力してください (10文字以下)");
        } else if (category.matches("^[  |　]+$")) {
        	errorMessages.add("カテゴリを入力してください");
        } else if (10 < category.length()) {
        	errorMessages.add("入力値を超えています (10文字以下で入力してください)");
        }

        if (StringUtils.isEmpty(text) == true) {
        	errorMessages.add("本文を入力してください (1000文字以下)");
        } else if (text.matches("^[  |　]+$")) {
        	errorMessages.add("本文を入力してください");
        } else if (1000 < text.length()) {
        	errorMessages.add("入力値を超えています (1000文字以下で入力してください)");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}