package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		//不正なパラメータ エラーセット
		List<String> alertMessages = new ArrayList<String>();
		String addressId = request.getParameter("userId");

		if ((StringUtils.isEmpty(addressId)) || !(addressId.matches("^[0-9]+$"))) {
			alertMessages.add("不正なパラメータが入力されました");
			session.setAttribute("alertMessages", alertMessages);
			response.sendRedirect("management");
			return;
		}

		//不当なIDに関わる不正なパラメータ
		String strEditUserId = request.getParameter("userId");
		int editUserId = Integer.parseInt(strEditUserId);
		User editUser = new UserService().select(editUserId);

		//ID直打ちによるユーザーチェック
		if (editUser == null) {
			alertMessages.add("不正なパラメータが入力されました");
			session.setAttribute("alertMessages", alertMessages);
			response.sendRedirect("management");
			return;
		}

		//System.out.println(selectedUser.getAccount() + ":" + selectedUser.getId());

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("editUser", editUser);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<String> errorSetMessages = new ArrayList<String>();

		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);

		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		String checkPassword = request.getParameter("checkPassword");

		User user = getUser(request);
		String strEditUserId = request.getParameter("userId");
		int editUserId = Integer.parseInt(strEditUserId);
		User editUser = new UserService().select(editUserId);

		if (isValid(user, editUser, checkPassword, errorSetMessages)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorSetMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorSetMessages.size() != 0) {
			request.setAttribute("errorSetMessages", errorSetMessages);
			request.setAttribute("user", user);
			request.setAttribute("editUser", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		session.setAttribute("editUser", user);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("userId")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(User user, User selectedUser, String checkPassword, List<String> errorSetMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		//アカウント名に関するエラー
		if (StringUtils.isEmpty(account)) {
			errorSetMessages.add("アカウント名を入力してください");

		} else if (account.matches("^[ |　]+$")) {
			errorSetMessages.add("アカウント名を入力してくださいを入力してください");

		} else if (6 > account.length()) {
			errorSetMessages.add("アカウント名は6文字以上で入力してください");

		} else if (20 < account.length()) {
			errorSetMessages.add("アカウント名は20文字以下で入力してください");
		} else if (!account.matches("^[A-Za-z0-9]+$")) {
			errorSetMessages.add("アカウント名は半角英数字で入力してください");

		} else if ((!selectedUser.getAccount().equals(user.getAccount()))
				&& new UserService().duplicateCheck(account)) {
			errorSetMessages.add("アカウントが重複しています");
		}

		//パスワードに関するエラー
		if (!password.equals(checkPassword)) {
			errorSetMessages.add("パスワードと確認用パスワードが不一致です");
		}

		//名前に関するエラー
		if (!StringUtils.isEmpty(name) && (10 < name.length())) {
			errorSetMessages.add("名前は10文字以下で入力してください");
		} else if (name.matches("^[ |　]+$")) {
			errorSetMessages.add("名前を入力してください");
		}

		//支社・部署の組み合わせ
		if (StringUtils.isEmpty(Integer.toString(branchId))) {
			errorSetMessages.add("支社を入力してください");
		}
		if (StringUtils.isEmpty(Integer.toString(departmentId))) {
			errorSetMessages.add("部署名を入力してください");
		}

		if ((branchId != 1) && (departmentId < 3)) {
			errorSetMessages.add("指定した支社では、選択した部署が存在しません");
		}
		if ((branchId == 1) && (departmentId > 2)) {
			errorSetMessages.add("指定した支社では、選択した部署が存在しません");
		}

		if (errorSetMessages.size() != 0) {
			return false;
		}
		return true;
	}
}