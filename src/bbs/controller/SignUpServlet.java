package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();

    	List<Branch> branches = new BranchService().select();
        request.setAttribute("branches", branches);

        List<Department> departments = new DepartmentService().select();
        request.setAttribute("departments", departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();

    	List<Branch> branches = new BranchService().select();
        request.setAttribute("branches", branches);

        List<Department> departments = new DepartmentService().select();
        request.setAttribute("departments", departments);

        String checkPassword = request.getParameter("checkPassword");
        User user = getUser(request);

        if (!isValid(user, checkPassword, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }

        new UserService().insert(user);
        response.sendRedirect("management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, String checkPassword, List<String> errorMessages) {

        String account = user.getAccount();
        String password = user.getPassword();
    	String name = user.getName();
    	int branchId = user.getBranchId();
    	int departmentId = user.getDepartmentId();

        User duplicateUser = new UserService().select(account);

        //重複エラー
        if(duplicateUser != null) {
        	errorMessages.add("アカウントが重複しています");
        }

        //アカウント名に関するエラー
        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }else if(6 > account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        }

        //パスワードに関するエラー
        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }else if(20 < password.length()) {
            errorMessages.add("パスワードは20文字以下で入力してください");
        } else if(6 > password.length()) {
        	errorMessages.add("パスワードは6文字以上で入力してください");
        }

        if (!password.equals(checkPassword)) {
            errorMessages.add("パスワードと確認用パスワードが不一致です");
        }
        //名前に関するエラー
        if (!StringUtils.isEmpty(name) && (10 < name.length())) {
            errorMessages.add("名前は10文字以下で入力してください");
        }

        //支社・部署の組み合わせ
		if (StringUtils.isEmpty(Integer.toString(branchId))) {
			errorMessages.add("支社を入力してください");
		}
		if (StringUtils.isEmpty(Integer.toString(departmentId))) {
			errorMessages.add("部署名を入力してください");
		}
        if((branchId != 1) && (departmentId < 3)) {
        	errorMessages.add("指定した支社では、選択した部署が存在しません");
        }
        if((branchId == 1) && (departmentId > 2)) {
        	errorMessages.add("指定した支社では、選択した部署が存在しません");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}