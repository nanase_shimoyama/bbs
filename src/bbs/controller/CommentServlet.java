package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Comment;
import bbs.beans.User;
import bbs.service.CommentService;


@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorComments = new ArrayList<String>();

        String text = request.getParameter("text");
        if (!isValid(text, errorComments)) {
            session.setAttribute("errorComments", errorComments);
            response.sendRedirect("./");
            return;
        }

        Comment comment = new Comment();
        comment.setText(text);

        String message = request.getParameter("message_id");
        comment.setMessageId(Integer.parseInt(message));

        User user = (User) session.getAttribute("loginUser");
        comment.setUserId(user.getId());

        new CommentService().insert(comment);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorComments) {

        if (StringUtils.isEmpty(text)) {
            errorComments.add("コメントを入力してください");
        } else if (text.matches("^[ |　]+$")) {
        	errorComments.add("コメントを入力してください");
        }else if (500 < text.length()) {
        	errorComments.add("500文字以下で入力してください");
        }

        if (errorComments.size() != 0) {
            return false;
        }
        return true;
    }
}