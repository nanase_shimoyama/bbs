package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;
import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {

		request.setCharacterEncoding("UTF-8");

      User user = (User) request.getSession().getAttribute("loginUser");

	  String strStart = request.getParameter("startDate");
	  String strEnd = request.getParameter("endDate");
	  String keywordCategory = request.getParameter("keywordCategory");

      List<UserMessage> messages = new MessageService().select(strStart, strEnd, keywordCategory);
      List<UserComment> comments = new CommentService().select();

      request.setAttribute("loginUser", user);
      request.setAttribute("messages", messages);
      request.setAttribute("comments", comments);
      request.setAttribute("strStart", strStart);
      request.setAttribute("strEnd", strEnd);
      request.setAttribute("keywordCategory", keywordCategory);
      request.getRequestDispatcher("/home.jsp").forward(request, response);
  }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String strStart = request.getParameter("startDate");
		String strEnd = request.getParameter("endDate");
		String keywordCategory = request.getParameter("keywordCategory");

		List<UserMessage> messages = new MessageService().select(strStart, strEnd,keywordCategory);

		HttpSession session = request.getSession();
		session.setAttribute("messages", messages);
		response.sendRedirect("./");
	}
}