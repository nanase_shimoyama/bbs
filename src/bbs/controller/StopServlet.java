package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet("/stop")
public class StopServlet extends HttpServlet {

	 @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws IOException, ServletException {

	        int isStopped = Integer.parseInt(request.getParameter("is_stopped"));
	        int selectedUserId = Integer.parseInt(request.getParameter("userId"));

	        User selectedUser = new UserService().select(selectedUserId);
	        selectedUser.setIsStopped(isStopped);

	        new UserService().restoreStop(selectedUser);

	     response.sendRedirect("./management");

	 }

}