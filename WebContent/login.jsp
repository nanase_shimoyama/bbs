<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="/css/allpage.css" rel="stylesheet" type="text/css">
        <title>ログイン画面</title>

            <c:if test="${ not empty loginErrorMessages }">
                <div class="loginErrorMessages">
                    <ul>
                        <c:forEach items="${loginErrorMessages}" var="loginErrorMessages">
                            <li><c:out value="${loginErrorMessages}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
    </head>
    <body>
        <div class="main-contents">

            <form action="login" method="post"><br />
                <label for="account">アカウント</label>
                <input name="account" id="account"/><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/><br />

                <input type="submit" value="ログイン" /><br />
            </form>

            <div class="copyright"> Copyright(c)shimoyama_nanase</div>
        </div>
    </body>
</html>