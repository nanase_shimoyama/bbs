<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="/css/allpage.css" rel="stylesheet" type="text/css">
			<script type = "text/javascript">
				function deleteCheck(){
					deleteCheck = window.confirm("本当に削除しますか？");
					if (deleteCheck == true){
							alert("削除しました");
							return true;
					}else {
							alert("キャンセルされました");
  	  						return false;
  	  				}
				}
			</script>
	</head>

	<body>
		<div class="main-contents">
		<div class="header">


        <c:if test="${ not empty loginUser }">
        		<a href="message">新規投稿</a>
						<c:if test  = "${loginUser.branchId == 1 && loginUser.departmentId == 1 }">
        					<a href="management">ユーザー管理</a>
        				</c:if>
        		<a href="logout">ログアウト</a>
        </c:if>
		<br />
		<br />
		<br />
        <c:if test="${ not empty errorComments }">
         	<div class="errorComments">
        		<c:forEach items="${errorComments}" var="errorComments">
            		<li><c:out value="${errorComments}" />
        		</c:forEach>
		    </div>
		    <c:remove var="errorComments" scope="session" />
		</c:if>
		<br />
		<c:if test="${ not empty errorMessages }">
         	<div class="errorMessages">
        		<c:forEach items="${errorMessages}" var="errorMessages">
            		<li><c:out value="${errorMessages}" />
        		</c:forEach>
		    </div>
		    <c:remove var="errorMessages" scope="session" />
		</c:if>
		<br />
		</div>
		</div>

		<div class="search">

		<form action="index.jsp" method="get">
		<p>---------<b>絞り込み検索</b>--------------------</p>
			<label for="date">日付</label>
				<input type="date" name="startDate" id="date"  value ="${strStart}"/>
				～
				<input type="date" name="endDate" id="date" value ="${strEnd}"/>
				<br />
				<label for="keywordCategory">カテゴリ</label>
				<input name="keywordCategory" id="keywordCategory"  value="${keywordCategory}" />
				<input type="submit" value="検索">
		<p>------------------------------------------------</p>
		<br />
		<br />
		<br />
        </form>
        </div>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
            		<div class="name"><c:out value="${message.name}" /></div>
					<div class="title"><c:out value="${message.title}" /></div>
					<div class="category"><c:out value="${message.category}" /></div>
					<c:forEach var="text" items="${fn:split(message.text, '
                       ')}">
                           <div class="text"><c:out value="${text}"/></div>
                       </c:forEach>
					<div class="createdDate"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            		<c:if test="${loginUser.id == message.userId}">
            		<form action="deleteMessage" method="post" onSubmit="return deleteCheck()">
						<input type="hidden" name="id" value="${message.id}">
						<input type="submit" value="削除">
					</form>
					</c:if>
					</div>

			<c:forEach items="${comments}" var="comment">
				<c:if test="${comment.messageId == message.id}">
					<div class="comment">
						<div class="name">
							<div class="name"><c:out value="${comment.name}" /></div>
							<c:forEach var="text" items="${fn:split(comment.text, '
								')}">
							<div class="text"><c:out value="${text}"/></div>
                       		</c:forEach>
            			<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					<c:if test="${loginUser.id == comment.userId}">
					<form action="deleteComment" method="post" onSubmit="return deleteCheck()">
            			<input type="hidden" name="commentId" value="${comment.id}">
            			<input type="submit" value="コメント削除">
            		</form>
            		</c:if>
            		</div>
            		</div>
				</c:if>
			</c:forEach>

				<form action="comment" method="post">
            			コメント記入欄<br/>
            			<textarea name="text" cols="100" rows="5" class="comment-box"></textarea>
            			<input type="hidden" name="message_id"value="${message.id}">
            			<br />
            			<input type="submit" value="コメント">
        			</form>
        	</c:forEach>
		</div>


		<div class="copyright"> Copyright(c)shimoyama.nanase</div>
    </body>
</html>