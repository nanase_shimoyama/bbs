<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
			<script>
				function switchUserConfirm(){
					if(window.confirm('このユーザーの状態を変更しますか？')){
						return true;
					}else{
						return false;
					}
				}
        </script>

    </head>

    <body>
        <div class="main-contents">
            <div class="header">
            <p>ユーザー管理画面</p>
                <a href="./">ホーム</a>
                <a href="signup">ユーザー新規登録</a>
				<br/>
				<br/>
				<c:if test="${ not empty alertMessages }">
                <div class="alertMessages">
                    <ul>
                        <c:forEach items="${alertMessages}" var="alertMessages">
                            <li><c:out value="${alertMessages}" />
                        </c:forEach>
                    </ul>
               		<c:remove var="alertMessages" scope="session" />
                </div>
				</c:if>

				<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
			<br/>
            </div>
			<br/>

        <div class="users">
           <c:forEach items="${users}" var="user">
           <div class="user">
				<div class="acount"><c:out value="${user.account}"/></div>
				<div class="branch_name"><c:out value="${user.branchName}"/></div>
				<div class="department_name"><c:out value="${user.departmentName}" /></div>
				<div class="name"><c:out value="${user.name}" /></div>
				<br />
           		<form action="setting" method= "get">
					<input type="hidden" name="userId" value="${user.id}" />
					<a href="setting"><input type="submit" value="編集"></a>
				</form>
 			 </div>


			<c:if test="${user.id != loginUser.id}">
				<form action="stop" method="post" onSubmit="return switchUserConfirm()">
    				<input type="hidden" name="userId" value="${user.id}" />
					<c:if test="${user.isStopped == 0 }" >
						<input type="hidden" name="is_stopped" value="1" />
						<input type="submit" value="停止">
					</c:if>

					<c:if test="${user.isStopped == 1 }" >
						<input type="hidden" name="is_stopped" value="0" />
						<input type="submit" value="復活">
					</c:if>
				</form>
			</c:if>
				</c:forEach>
				</div>
		</div>


            <div class="copyright">Copyright(c)Shimoyama_nanase</div>
    </body>
</html>