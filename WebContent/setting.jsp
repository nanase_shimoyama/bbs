<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集画面</title>
		<script>
				function editUserConfirm(){
					if(window.confirm('このユーザーを変更しますか？')){
						return true;
					}else{
						return false;
					}
				}
        </script>

		<c:if test="${ not empty alertMessages }">
                <div class="alertMessages">
                    <ul>
                        <c:forEach items="${alertMessages}" var="alertMessages">
                            <li><c:out value="${alertMessages}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="alertMessages" scope="session" />
		</c:if>

		<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
		</c:if>

		<c:if test="${ not empty errorSetMessages }">
                <div class="errorSetMessages">
                    <ul>
                        <c:forEach items="${errorSetMessages}" var="errorSetMessages">
                            <li><c:out value="${errorSetMessages}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorSetMessages" scope="session" />
		</c:if>
    </head>

    <body>
        <div class="main-contents">


        <form action="setting" method="post"><br />
                <input name="userId" value="${editUser.id}" id="id" type="hidden"/>

                <label for="account">アカウント名</label>
                <input name="account" value="${editUser.account}" /><br />

				<label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="checkPassword">確認用パスワード</label>
                <input name="checkPassword" type="password" id="checkPassword"/> <br />

                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/><br />

                <c:if test="${loginUser.id != editUser.id}">
					<label for="branchId">支社名</label>
						<select name = "branchId">
							<c:forEach items="${branches}" var="branch">
					  		<c:if test="${editUser.branchId == branch.id}">
								<option value = "${branch.id}" selected>
								<c:out value="${branch.name}" />
								</option>
                       		</c:if>
                       			<c:if test="${editUser.branchId != branch.id}">
                          			<option value="${branch.id}">
                             		<c:out value="${branch.name}" />
                          			</option>
                       			</c:if>
                    		</c:forEach>
                  		</select><br />

				<label for="departmentId">部署名</label>
				<select name = "departmentId">
					<c:forEach items="${departments}" var="department">
					   <c:if test="${editUser.departmentId == department.id}">
						<option value = "${department.id}"><c:out value="${department.name}" /></option>
						    <c:out value="${department.name}" />
						</c:if>
                      		<c:if test="${editUser.departmentId != department.id}">
                         	<option value="${department.id}">
                            	<c:out value="${department.name}" />
                         	</option>
                      		</c:if>
                    </c:forEach>
                  </select><br />

			</c:if>

				<c:if test="${loginUser.id == editUser.id}">
                   <input type="hidden" name=branchId value="${editUser.branchId}" />
                   <input type="hidden" name="departmentId" value="${editUser.departmentId}" />
                </c:if>
                <input type="submit" value="更新" /> <br />
                <a href="./management">戻る</a>
        </form>

            <div class="copyright"> Copyright(c)Shimoyama_Nanase</div>
        </div>

    </body>
</html>