<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿画面</title>
         <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
                <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                <c:remove var="errorMessages" scope="session" />
                </div>
				</c:if>
                </div>

<div class="form-area">

	<form action="message" method="post">
     		<label for="title">タイトル</label>
			<input name="title" value="${message.id}" id="account"/><br/>
			<br/>
			<label for="category">カテゴリ</label>
			<input name="category" value="${message.category}"/><br/>
			<br/>
			本文<br/>
            <textarea name="text" cols="100" rows="10" class="text-box"></textarea>
			<br />
            <input type="submit" value="投稿">
            <a href="./">戻る</a>
	</form>
</div>



            <div class="copyright"> Copyright(c)nanase.shimoyama</div>
    </body>
</html>