<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録</title>
                <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
    </head>

    <body>
		<div class="main-contents">
		<div class="resister">
		<form action="signup" method="post"><br />
            <p>ユーザー新規登録</p>
                <label for="account">アカウント名</label>
                <input name="account" id="account" value="${user.account}" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="checkPassword">確認用パスワード</label>
                <input name="checkPassword" type="password" id="checkPassword" /> <br />

                <label for="name">名前</label>
                <input name="name" id="name" value="${user.name}"/><br />

				<label for="branchId">支社名</label>
				<select name = "branchId">
					<c:forEach items="${branches}" var="branch">
						<option value = "${branch.id}"><c:out value="${branch.name}" /></option>
					</c:forEach>
				</select><br/>

				<label for="departmentId">部署名</label>
				<select name = "departmentId">
					<c:forEach items="${departments}" var="department">
						<option value = "${department.id}"><c:out value="${department.name}" /></option>
					</c:forEach>
				</select><br/>

                <input type="submit" value="登録" /> <br/>
                <a href="./management">戻る</a>
            </form>

            <div class="copyright">Copyright(c)Shimoyama_nanase</div>
        </div>
    </div>
    </body>
</html>